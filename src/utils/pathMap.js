/**
 * 接口基地址
 */
export const BASE_URI = 'http://157.122.54.189:9089';

/**
 *  登录 获取验证码
 */
export const ACCOUNT_LOGIN = '/user/login'; // 登录

/**
 * 检查验证码
 */
export const ACCOUNT_VALIDATEVCODE = '/user/loginVerification'; // 检查验证码
