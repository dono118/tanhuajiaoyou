import React, {Component} from 'react';
import {Text, StyleSheet} from 'react-native';

import {CodeField, Cursor} from 'react-native-confirmation-code-field';

class App extends Component {
  state = {
    vcodeTxt: '',
  };
  onChangeVcodeTxt = (vcodeTxt) => {
    this.setState({vcodeTxt});
  };
  render() {
    return (
      <CodeField
        value={this.state.vcodeTxt}
        onChangeText={this.onChangeVcodeTxt}
        cellCount={6}
        rootStyle={styles.codeFieldRoot}
        keyboardType="number-pad"
        textContentType="oneTimeCode"
        renderCell={({index, symbol, isFocused}) => (
          <Text
            key={index}
            style={[styles.cell, isFocused && styles.focusCell]}>
            {symbol || (isFocused ? <Cursor /> : null)}
          </Text>
        )}
      />
    );
  }
}

const styles = StyleSheet.create({
  codeFieldRoot: {marginTop: 20},
  cell: {
    width: 40,
    height: 40,
    lineHeight: 38,
    fontSize: 24,
    borderBottomWidth: 2,
    borderColor: '#00000030',
    textAlign: 'center',
    color: '#7d53ea',
  },
  focusCell: {
    borderColor: '#7d53ea',
  },
});

export default App;
